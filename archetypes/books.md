---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
started: {{ .Date }}
openlibrary: https://openlibrary.org/books/
shelves:
  - biography
  - business
  - comedy
  - fantasy
  - fiction
  - finance
  - history
  - non-fiction
  - note taking
  - philosophy
  - productivity
  - sci-fi
  - science
  - self-help
  - software engineering
  - spirituality
  - writing
draft: true
---
