<div class="center">
    <img style="max-width: {{ .Get "width" }}" src="{{ .Get "file" }}" alt="{{ .Get "file" }}" />

<p>{{ .Inner | markdownify }}</p>
</div>
