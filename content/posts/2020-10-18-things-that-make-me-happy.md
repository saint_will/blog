---
title: "The things that make me happy"
slug: things-that-make-me-happy
date: 2020-10-17
keywords: [minimalism, lifestyle, durability]
categories: [lifestyle]
summary: These nine things cost all under $300 but the value they bring is much higher.
---

In the [last post](/posts/pot/), I got excited about getting a pot for my birthday. I got positive feedback (thanks Ben) and thought that I might list **nine items** that I often use and that make my life better.

## 100% Cotton sweatpants

Being in comfortable clothes is underestimated. Since I moved out to go to uni, I wore jeans or chinos at all times. I thought that sweatpants are only for rappers, hardcore slavs and gym junkies. I still wouldn't go out in my sweatpants, and I feel mildly judgemental about people who do. But at home? Nothing can beat the comfort of good-quality sweatpants on a cold day.

I got the last two pairs from my girlfriend. She bought them in Terranova.

{{<center-image width="330px" file="/images/posts/things-that-make-me-happy/pants.png">}}
*Credit: [terranovastyle.com](https://www.terranovastyle.com/cz_cz/kalhoty-teplakove-jednobarevne-23-sab0042121001s047-kw-pantalone-ginnico-lungo-kw-grigio/).*
{{</center-image>}}

## Bose QuiteComfort 35 II

I mentioned these headphones before, and I should probably get some stock options from Bose for how much I promote them. I came across these for the first time when my ex-colleague Timothy let me try them up back in our Melbourne office. It was one of those massive open spaces where over a hundred people work on a floor without single doors, just a few barriers here and there. I put the headphones one and there it was: a quiet comfort. I bought myself my first pair the next day. That was in 2018, and the headphones were with me almost everywhere since then.

They saved me from crying babies on an aeroplane, from drunk teenagers on a bus, and from hostel parties in Thailand (when I was hungover from the previous-day party).

{{<center-image width="400px" file="/images/posts/things-that-make-me-happy/bose-qc-35.png">}}
*Credit: [www.bose.com](https://www.bose.com/en_us/products/headphones/over_ear_headphones/quietcomfort-35-wireless-ii.html#v=qc35_ii_black).*
{{</center-image>}}

## PocketBook 740

Reading makes me happy. This ebook reader mediates that so I'm including it. It's got 7.8" screen with backlight and it supports any document format that you throw at it. Those were the main reasons why I upgraded from my Kindle Paperwhite.

But PocketBook is mainly a hardware company and so the reader software looks and feels a bit dated, and it never gets updated.

I use the reader often, but it's on the top of my "upgrade with something new" list. Maybe [ONYX BOOX NOVA 2](https://onyxboox.com/boox_nova2).

## Google Pixel 2 XL

October 19, 2017. That was the day when Google released the best phone I've ever owned. The battery life brought back the memories of my beloved Nokia 3310, the fast charge was still a new thing, and I'm regularly delighted when I plug my Pixel to a charger minutes before I leave the house, and I get enough charge for the rest of my day.

Almost three years after I bought mine, I'm starting to look for a new phone, but the shoes to fill are large. I think I'll still survive 2021 with this sweetheart.

## Screen arm

The look, the feel and the comfort of having your monitor attached to the tabletop with a dynamically positionable arm is something I can't work without. It makes space on your desk and allows your head to be always in the right position.

I got one inexpensive from a Czech supplier.

{{<center-image width="300px" file="/images/posts/things-that-make-me-happy/arm.jpg">}}
*Credit: [alza.cz](https://www.alza.cz/alzapower-ergoarm-ar700-usb-d5346293.htm?o=33).*
{{</center-image>}}

## Skinners

Skinners are the brand of my barefoot shoes. They are not as much shoes as half-rubber socks. I bought the skinners because they were the cheapest barefoot "shoes" I could find. So far I'm quite happy with them even though the waterproofness of the sole lasted only around 80km of walking.

The point in wearing barefoot shoes for me is that it makes walking in nature more fun, you notice what surface you walk on, and you've got to be more present otherwise you are going to get brought back to presence by sharp rocks and sticks.

{{<center-image width="400px" file="/images/posts/things-that-make-me-happy/skinners.png">}}
*Credit: [skinners.cc](https://skinners.cc/en/).*
{{</center-image>}}

## Feather jacket

Feather jacket, similarly to the Bose headphones, is something you don't know you need till you get the chance to try it.

In my early twenties, I went on a mountain-climbing trip to Russian Caucasus mountains. When we were hiking up [Elbrus](https://en.wikipedia.org/wiki/Mount_Elbrus), we started the last day of the ascent at 2 AM. It was so cold that the water in my water bottle froze. Normal layered clothes with Gore-Tex jacket was enough for going up, but when we made our rest stops, I thought I'm going to freeze. My friends just pulled their feather jackets out of their backpacks and put them over all other clothes.

The advantage of a feather jacket is that it's incredibly light, compact and insulating. You don't need to climb mountains to benefit from having one.

I've got three feather jackets, but the one that makes me happiest is Mountain Equipment Helium. It weighs a little over 100g (3.5oz). It compacts into a grapefruit-sized bundle. They are not making my model any more. Here's a link to [the newer version](https://www.mountain-equipment.co.uk/collections/helium-technology/products/odin-jacket).

{{<center-image width="400px" file="/images/posts/things-that-make-me-happy/helium.jpg">}}
Mountain Equipment Helium
{{</center-image>}}

## TRX and elastic bands

I like gyms better than owning all the gym equipment. That's it. I don't like going to the gym, I'm a shy exerciser next to other people and sometimes, but not often, I get annoyed by someone else's behaviour.

Since even before the pandemic, I would quite often just settle for exercising with my own weight at home. But certain muscles can't be worked out without additional equipment. On top of that, it's hard to increase the intensity with bodyweight exercises.

The answer for me is TRX (suspended straps) and elastic bands. It pleases my minimalistic nature that I can fit my whole gym into a small bag.

{{<center-image width="400px" file="/images/posts/things-that-make-me-happy/band-trx.jpg">}}
Bands and TRX
{{</center-image>}}

## Cable organiser

The last thing on today's list is a cable organiser. I bought mine before I went on nine months of travelling, but I use it often ever since. It replaced a big drawer filled with a mess of cables. The price of lost charging USB cables, USB sticks and headphones alone probably paid tenfold for it.

{{<center-image width="400px" file="/images/posts/things-that-make-me-happy/bubm-organizer.jpg">}}
*Credit: [BUBM Organizer Roll UP](https://www.aliexpress.com/item/32480976632.html).*
{{</center-image>}}

I hope this list inspired you to reward yourself with something nice and useful. There are no affiliate links here. Let me know what makes you happy at me@viktomas.com.
