---
title: "How my article became one-hit-wonder on hacker news"
slug: one-hit-wonder-on-hn
date: 2020-07-05
keywords: [writing, lifestyle]
categories: [lifestyle]
summary: How my article reached the hacker news front page. And how it stayed there for two whole days.
---

Today I'm going to tell the story of fleeting success. You'll learn how my article reached the hacker news front page. And how it stayed there for two whole days, changing my perception about what is achievable with my writing hobby.

{{% theme-image width="300px" filename="one-hit-wonder-on-hn.svg" %}}

I started this blog as a new year's resolution for 2020. One article per week. I didn't have gigantic aspirations. I wanted to learn how to express myself better because [remote work](/posts/remote/) is all about written communication. And I wanted to share thoughts about my interests.

First two months I didn't publish the articles I wrote. I didn't want to create yet another blog with only a handful posts in it. The writing had to become a habit first.

After I started publishing on this site, I didn't share it with anybody until late April when I mentioned [an article](/posts/plaintext-passwords/) on [hacker news](https://news.ycombinator.com/item?id=22914281) for the first time. Two people liked it, and about fifty read it according to my analytics.

On Sunday 3rd of May 2020 at 9 AM (CET) I mention my article ["What would you do if you lost your Google account?"](/posts/losing-google-account/) on [hacker news](https://news.ycombinator.com/item?id=23057365). I was expecting the same results: a couple dozen people read it, a few would like it. But with strike of luck, plenty of people liked the article and of it went to the front page.

By 10 AM, fifteen hundred people visited the site, four times the number of visits since I started the blog. In the following 24 hours, another ten thousand people viewed the blog, and by the time the article left the front page on Monday, **sixteen thousand people** have read it[^1]. The real number is going to be over thirty thousand because [60% of hacker news readers don't show in analytics thanks to AdBlock](/posts/adblock-skews-analytics/).

[^1]: The numbers are from Mixpanel analytics. I copied them from a conversation with my friend because the Mixpanel stats have been long lost thanks to me exceeding the monthly limit.

I was ecstatic. The whole day I was walking with a wide grin on my face. I immediately googled the WikiHow article on [How To handle fame](https://www.wikihow.com/Handle-Fame). And I took plenty of screenshots because I thought that the article is bound to drop off from the front page after a few minutes and I wanted to have a memory.

![](/images/posts/one-hit-wonder-hn/medium.gif)

There was a trickle of people coming to see the article ever since. Now about fifty people view the article each week.

The time of my two-day fame has now passed, but I took away valuable lessons.

The first lesson is that I can produce something of value outside of my programming job. I knew I design and write high-quality code and systems and I get well paid for doing that. However, this experience showed me that I could create something else people enjoy.

The second lesson is the type of article people enjoy. The generic articles about [FOSS](/posts/foss/) and [Meditation](/posts/meditation-introduction/) are presumably not as interesting as the more down-to-earth articles that contain a few howtos that people can apply straight away. Since the "loosing your google account" article this story repeated itself once more with the [Zettlekasten article](/posts/slip-box/). This strengthens my hypothesis that the article needs to provide useful, applicable tool/information to the readers, not just abstract food for thought.

I liked Robert Heaton's [Made-Up-Award principle](https://robertheaton.com/2019/09/24/how-to-come-up-with-blog-post-ideas/): you should write your article to be the best in some narrow category. Exaggerated example: "best introduction to ruby programming for marketing experts focusing on food marketing". Another important rule for me is that I have to wish I found that article a few days/weeks before because it would help me solve a problem.

This event gave me just enough extrinsic motivation to help me overcome the initial period when I was considering whether I want to spend eight hours each week writing an article that fifty people read. It helped me to commit to writing as the next of my many hobbies.
