---
title: "Zettelkasten note-taking after one year"
slug: slip-box-after-one-year
date: 2021-03-08
blockquote: highlight
keywords: [productivity, learning, notes]
categories: [productivity]
draft: true
---

X months ago, I've written a [popular post](/posts/slip-box/) about the Zettelkasten note-taking method. It was freshly after reading the [How to Take Smart Notes](/books/ahrens-sonke-how-to-take-smart-notes/). Even though the basic premise of the method is still the same, I'm not using the method the same way as I planned. This is my update.

## Why?

- [ ] include low context from the GitLab handbook

The "why" hasn't changed. I'm interested in the method because it is focused on building a long-lasting knowledge base. It's based on short, atomic notes about one specific topic that are connected with each other using links. These notes are optimized for read which means that they take much more time to write because you are explaining the topic with [low context].

Thanks to the interlinking between notes you need to these important questions for each new note:

- How does it fit into my existing knowledge?
- Does this piece of information conflict with my previous understanding?

Answering these questions takes even more time, but it will pay off in both the quality of your understanding and the ease of reading in the future.

## What?

Should you store a piece of information in your slip-box or should you put it somewhere else?

I store textual information in two places, Joplin and my note system (more on that later). Joplin is a great open-source alternative to Evernote and I use it for my day-to-day notes like TODOs, notes about my finances, articles in progress and anything [Getting Things Done](/posts/gtd-concepts/) related.

When I learn something that I would like to write about or I would like to know that five years from now, I put it in my notes system. My thinking is that my note system should be eventually reflection of the important knowledge stored in my brain.

## How?

Now the most interesting part: How do I store my notes? 

- [ ] do the zettelkasten authors give some reasons why wikilinks are better than markdown links?

I started taking notes with [Zettlr]. It is an open source note-taking app that heavily relies on WikiLinks (`[[202103100852]]`). Over time I started longing for a more universal format, because the WikiLinks are need custom software to work. I didn't see any reason not to use plain Markdown. It supports liking out of the box and there are countless available tools to write and process it.

So I left Zettel for [VS Code]. VS Code is a text editor for developers and using it is more natural to me. I'm not the only one going in this direction, there are [FoamBubble] and [Dendrite] projects that are also customising VS Code for note taking.

I developed [an extension] that simplifies the most common tasks like creating and linking notes. I also use [Markdown Links] to visualise relationships between my notes.

How I create a note:

1. I start reading a book and I create a new reference note
1. I write many sentences that phrase my understanding of the content of a resource
1. I group related sentences by a topic
1. I use [Slipbox] to create a new Permanent note for each interesting group of thoughts
1. I paste the group sentences in the permanent note
1. I look for related permanent notes to find where I can link the new note
1. I write the new permanent note in different words but to still capture the main thought

## Main issue?

My main issue is the time it takes to write one permanent note. I was reading [Software Engineering at Google] and it took me three hours to read a chapter and make five permanent notes out of it. I'm comforting myself that now that I put effort to extract the knowledge and link it with my existing notes, I'll always have it easily available. But are 3 horus worth it?

## Main takeaways for the reader

- [ ] Why - because I believe in permanent notes that are going to be useful in ten years
- [ ] What - only things that I will want to read in five years
	- [ ] GTD notes in Joplin, the deciding factor is "is this going to be a useful text in five years"
- [ ] How - VS Code, Markdown, not perfect
	- [ ] Perfect is the enemy of good - I started with having around 70 fantastic notes and then I fell that unless I keep the same standard, I'm going to polute my notes
	- [ ] Plain markdown is still my preferred format - this might change but now I just like markdown files that are linking between each other. The software is a bit defficient
	- [ ] VS Code with markdown links is good enough

## Outline

```mermaid
graph TD;
A["starting with the method"] --> B["current setup"]
B --> C["The good, the bad, and the ugly"]
```

1. Starting
	1. I wrote a about slipbox on 7th of June 2020
	2. The main alure was the idea of optimizing the notes for read and making them permanent
	3. I started with Zettel
	4. I disliked the wiki links with number timestamps in them. It seemed like an arbitrary format
2. Current state
	1. Move from Zettel to VS Code
		1. **Deciding on the format - linked markdown files**
		2. **Versioned with git**
		3. Implemented my own extension
	4. Seeing the foam and **taking the markdown links**
		1. Not keen on lock-in to such a short-lived project
		2. There is also dendrite and org-roam. I wish I was an emacs user.
	6. The rate of taking new notes started slowing down
		1. I started thinking that this piece of inforamation is not good enough to put into my note base
		2. I measured all new notes against the "perfectly linked" and processed initial 50
	7. I put some notes in Joplin and some in my notes system
		1. The deciding factor is "**Will I need this note in five years time, will I write about it?**"
6. The good, the bad, and the ugly
	1. I stll strugle with finding the minimal note size (e.g. "should I put a two-sentence piece of information as a new note")
	2. Writing good notes takes **forever** - it easily doubles the timem it takes me to read a book
	3. I haven't found software that would allow me to get a quick overview of 5-10 notes. Opening that many split-tabs in VS Code is a pain
		1. The closest is the `cortex` hugo theme
	4. I'm still in love with the premise of having notes that are going to be alive and active in 5 years time. I never re-visitted notes before I found zettelkasten
	5. Unlike the Zettelkasten, I'm going back and updating existing notes, rather than creating new notes that would be meant to replace the original note
