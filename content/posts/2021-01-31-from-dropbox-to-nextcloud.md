---
title: "Switching from Dropbox to Nextcloud"
slug: from-dropbox-to-nextcloud
date: 2021-01-31
keywords: [dropbox, nextcloud, cloud, storage, privacy]
categories: [techsoftware]
summary: Nextcloud is self-hosted, open-source, cheaper alternative to Dropbox.
---

Instead of a new year's resolution, I've done a new year's cleaning in 2021. My Dropbox subscription is about to expire in early February, and I had to decide whether I want to keep paying $120 for Dropbox or try something new. And so the first month of the year became a period of digital cleansing.

{{% theme-image width="450px" filename="from-dropbox-to-nextcloud.jpg" %}}

## My history with Dropbox

I've been a Dropbox user since uni, and for the last four years, I've been paying for Dropbox Plus. I still remember when I first installed it, back then on Windows 7. I couldn't believe how seamless the experience felt. I think I read the Dropbox origin story in Lean Startup. The way Dropbox worked was so novel that the founders had to create a video to illustrate how is Dropbox going to work. Only words wouldn't do because people couldn't imagine the seamlessness in the time of FTP and Samba share.

At that time, Dropbox was the synonym for innovation, [Mailbox](https://www.theverge.com/2015/12/8/9873268/why-dropbox-mailbox-shutdown), [Paper](https://www.dropbox.com/paper), I've used it all, and I loved it. But in recent years, I started noticing that I use a constant portion of the features. I want to

- Backup my files
- Access them from my Android phone
- Share the files with other people through links
- **Share folders with friends and family** (giving them write access)

## Dropbox stopped being the best option for me

It's nice to have the time-machine feature for additional safety, but I've never used it. It is convenient to scan documents directly into dropbox, but now there are better apps for that. All in all, I want cloud storage that's available from my phone, nothing fancy.

I started noticing drawbacks that made me look for alternatives. Some of those drawbacks were:

- The Linux support is working, but it's not all that great on Arch
- I  only used a small fraction of the 2TB that I was paying for (now I use 100GB)
- It was hard to share large folders with people who didn't have paid Dropbox
- I'd prefer to have my data stored in the EU, rather than the US
- Dropbox is closed-source

## Hello Nextcloud

When I looked at the alternatives, I found Nextcloud. Popular fork of ownCloud and fully open-source[^1] cloud storage platform. Nextcloud doesn't offer hosting for individual users, but other providers do thanks to the permissive license. With a bit of Reddit's help[^2], I found [Hetzner](https://www.hetzner.com/storage/storage-share) a German hosting provider that can spin up a NextCloud instance for you at a very reasonable price (5.8 EUR/month for 500GB). The benefit of Nextcloud over ownCloud is that there are no restricted features. You get the full feature set of the enterprise version in every Nextcloud deployment.

Creating a new Hetzner account takes a few minutes and then a few more till your new Nextcloud installation starts. You get an email with your admin login, and you can start creating normal users. Creating multiple users was a fantastic change from Dropbox where you need to pay an extra $80/year for more users[^3]. I immediately made an account for myself and my girlfriend.

So far I'm happy with the move. The synchronising seems to be slower than with Dropbox. Nextcloud doesn't hash the file binary content, and whatever else it does, it takes a long time to validate that my 100GB of data is fully synchronised. This situation improved when I removed a few git repositories and `node_modules` folders, but I didn't have to do that with Dropbox.

I also must admit that I am a bit more hesitant to blindly trust the Hetzner data availability as much as I did to Dropbox. That means that I replicate the Nextcloud data to one of my external hard drives as well. That's not a problem though because when I migrated all my data from Dropbox, I reduced the overall size to 20% by deleting some of the files that I thought I could go back to "someday".

## What's next

One of the exciting Nextcloud features is natively supported [End to End encryption](https://nextcloud.com/endtoend/). So far, I've been using [Cryptomator](https://cryptomator.org/) to securely store some of my data like bank statements and scanned ids. But using Nextcloud directly will reduce complexity.

## Should you use Nextcloud

I'm not planning on going back, but this move is surely not for everyone. If you don't use Linux, don't have a preference for open-source, use Windows or Mac and want your cloud data storage to "just work", or you'll use all the additional features Dropbox offers, then you are better off paying a bit extra and keep using Dropbox.

[^1]: APGPLv3 license
[^2]: [Is Hetzner a reliable option to host Nextcloud? : NextCloud](https://www.reddit.com/r/NextCloud/comments/fvedtj/is_hetzner_a_reliable_option_to_host_nextcloud/)
[^3]: Dropbox Family with up to 6 users.
[^4]: Nudge, nudge, wink, wink, say no more [Adobe Scan, PDF Scanner app for iPhone & Android](https://acrobat.adobe.com/us/en/mobile/scanner-app.html)
