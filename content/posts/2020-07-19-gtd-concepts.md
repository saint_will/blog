---
title: "Three principles distilled from Getting Things Done"
slug: gtd-concepts
date: 2020-07-19
keywords: [productivity, notes, gtd, books]
categories: [productivity]
blockquote: highlight
summary: Overview of the core ideas from a famous productivity book.
---

[Getting Things Done](/books/allen-david-getting-things-done/) (GTD) is a method of managing your work. David Allen published it 19 years ago as a result of his life's work coaching executives. The method takes time and effort to implement, but the key concepts behind it can benefit you today. This article explains the building blocks of GTD and how they help you clear your head from all unfinished work.  

{{% theme-image width="300px" filename="gtd-concepts.svg" %}}

## The core idea of GTD

> You can keep all unfinished tasks and thinking in an external note-taking system and not have them in your head.

Each one of the following concepts is helping you with moving the responsibility for remembering things from your mind to your system.

- **One place** - You store all your notes in one system with a clear structure. You separate notes that do and don't need your action.
- **Next Action format** - Any note that still needs your action has a specific structure.
- **Weekly revision** - You are continuously revising your notes, so no important note gets forgotten.

If your productivity system has these three elements, it will allow you to stop thinking about unfinished tasks, regardless of whether you follow GTD to the latter.

### One place

How many places would you now have to check to get all our outstanding work? Email, calendar, task list app on your phone, plain-text TODOs on your computer, paper notes, and your head. You can't guarantee that you are going to look to that one spot where you made the critical note. So you subconsciously can't let the important tasks out of your mind, and they will still occupy your mental space.

If you consciously replace all these systems with one, you don't have to worry about where to look for a note or whether you've got a complete picture. Combined with the next two principles, it will allow your mind to forget about the task temporarily and leave your note-taking system to keep track of it.

There is an exception to the one place principle: **Calendar**. Anything that has to happen at a particular time goes to your calendar. The calendar is just too damn good at keeping temporal information to try to substitute it by a note-taking app.

### Next Action format

David Allen introduces the concept of "Stuff". Stuff is anything that you need to do something about. You've made a commitment to yourself or to others to change something.

An example of stuff is *"Car needs fixing"* or just *"Car"*. If your TODO lists are like mine used to be, they are full of such reminders. *"Car"* tells you that something needs to be done, but it doesn't say what. The vagueness of such note helps you procrastinate.

Contrast that with the **Next Action** note format: What is the very next **physical** action that you need to take to move this thing forward? You need to be able to close your eyes and see yourself doing the physical activity and see the outcome.

*"Car"* changes to *"Call my friend Jim and ask him for contact information of the mechanic he was so happy with"*. Don't worry about what happens next. You'll come up with the Next Next Action after you've done this one.

You should often process all your "stuff" and turn it into next actions or archived notes for reference.

> Pro tip: If you come up with the next action and it takes less than two minutes, **do it straight away**[^1].

### Weekly revision

Regularly revisiting my notes is where my previous system lacked the most.

Revising your notes means archiving notes that you don't need to do anything about any more. It means organising the notes that need action into lists and picking the ones you can do something about the next week.

If you don't do this, there is a good chance that some important outstanding task gets lost in the older and dusty parts of your system. When that happens, your mind is going to realise that it can't trust the system, and it will take back the responsibility for remembering all outstanding work.

Without the weekly revision, you also don't know whether the next actions you work on are the most important work you could be doing. Maybe the note that you created a few weeks ago is now more important.

You should revisit your priorities and maybe put some things on a back burner, so you can focus on the things that matter now. This process takes just over an hour each week, but that's pennies when you consider how much mental space this activity frees up.

## GTD in detail

[The book](/books/allen-david-getting-things-done/) has dense 270 pages, and this summary contains only the principles that struck me the most. You can read the book if you are interested in the details of the method. If you don't have that much time, I recommend [Erlend Hamberg's GTD guide](https://hamberg.no/gtd/). It doesn't push any software agenda, and it's nicely illustrated.

I've implemented these concepts just under a month ago. Once I feel I have a good grasp of the practical details, I'm going to share much more practical workflow with the tools I'm using for my system.

[^1]: Read the book [Getting Things Done](/books/allen-david-getting-things-done/) from David Allen to get many more tips with deeper explanation.
