---
title: "Insurance is like gambling, don't overdo it"
slug: insurance
date: 2021-06-16
keywords: [insurance, personal finance, popular]
categories: [lifestyle]
tags: ["popular"]
summary: Insurance companies, like betting companies, need high margins to keep operating. You are paying that margin.
---

*Insurance companies, like betting companies, need high margins to keep operating. You are paying that margin.*

---

You can insure anything these days. From satellite launches to the screen on your smartphone. But not all insurances are created equal. When does it make sense to get insurance, and when will it only cost you extra money? This post describes what insurance is, and it might change your mind on whether you need it or not.

{{% theme-image width="600px" filename="insurance.webp" name="Insurance" %}}

The oxford dictionary defines insurance as:

> An arrangement by which a company or the state undertakes to provide a guarantee of compensation for specified loss, damage, illness, or death in return for payment of a specified premium.

The definition is a complex way of saying that you'll pay someone a small amount of money, and they promise to pay you much more if an improbable event happens. It's the same as a bet, but you don't want to win.

Betting and insurance have one thing in common. You pay more than what's your chance to win. The betting/insurance company needs money to pay salaries, bills, and shareholders.

## Scenario

Let's say that the chance your house burning down this year is 1 in 10,000 and that your house costs $100,000. That means that you should pay a $10 fee (called the premium) to insure your house against burning down.

```
premium = house value * chance it burns down
$10 = $100,000 * 0.0001
```

When 10,000 people insure their house, the insurance company collects $100,000 in fees. However, if the chance is correctly estimated, one of the houses is bound to burn down, and the insurance company will have to pay $100,000 to that one client. The insurance company won't have any money to pay the bills and will go bankrupt.

A profitable model is to ask for a $20 premium. That way, when the company pays the poor house owner $100,000, it will still have $100,000 left in the bank. From this extra money, they can pay bills and the agent who sold you the insurance.

Now we have a semi-realistic scenario. You paid $20 - twice as much as what is the chance. The key morale of the story is:

**Insurance companies (same as betting companies) always need to bill you more than the chance you get your big payoff.**

If this weren't the case, they would go bankrupt.

## Why pay insurance

Assuming you are not a betting man, or you are smart and bet only a small portion of your income as a fun activity, why would you pay for insurance (now that you know it's almost the same as betting)?

The answer is simple and important:

**Rule #1: Insure only against events that would take away your financial freedom.**

Assume you've got $10,000 saved up for any emergency, and you earn $40,000 a year.

| event | impact | should you insure? |
| -- | -- | -- |
| Your phone screen cracked. | You have to pay $200 for a new screen. | NO! |
| You crashed your car into a tree. | You have to pay $3,000 for repairs on your car. | Probably no, it's not ruining you financially. |
| You crashed your car into a Bentley on an intersection | $30,000 you must pay to the car owner + damage on your car. | YES, this sounds like it would set you back. You'd have to take a loan to pay for the damages. |
| Your house got damaged in a natural disaster. | Repairs will cost you over $100,000. | YES! it could take you many years to financially recover. |

## When insuring, pay as little as possible

One difference between betting and insuring is the excess. Excess is an amount of money that you have to pay first before you get the insurance payment. From the house example, if you set excess to $5,000:

| event | cost | you pay | insurer pays |
| --- | --- | --- | --- |
| Ground floor is flooded | $7,000 damage | $5,000 | $2,000 |
| The whole house burns down | $100,000 | $5,000 | $95,000 |
| Window breaks | $400 | $400 | $0, they won't even know |

The lower the excess, the higher the chance the insurance company needs to deal with you, the higher the premium.

Following Rule #1, we don't need to insure broken window or flooded rooms. That's what our savings are for.

**Rule #2, set the highest possible excess for your insurance**

You want to involve the insurance company only when the potential event causes you severe financial distress. You shouldn't involve the insurance company in events that you can easily cover from your savings. If you do, you have to pay much more than what's the chance of the event. I explained that at the beginning.

## Hustle if you want to

Lastly, I've read advice[^1] that you shouldn't just renew your yearly insurance subscription automatically but should call the insurance company and try to get a better deal each year. This sounds like good advice, but I haven't tried it yet.

Insurance is a costly betting that's only worth it if you can't afford to pay for the damage yourself. That means it should be reserved for expensive assets and for your health. You will unnecessarily lose money if you insure something that your savings can cover.

---

Of course, you are taking a larger risk by not insuring cheaper items. The risk is logically and financially worth taking because, by definition, it can't cause you severe problems. However, it's a risk non the less. And it is your own, **I take no responsibility**. If you cancel your iPhone screen insurance and drop it on a footpath tomorrow, pay for the repair knowing that you've done a smart thing.

[^1]: In [barefoot investor](https://www.barefootinvestor.com/).
