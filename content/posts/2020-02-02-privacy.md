---
title: "Privacy"
slug: privacy
date: 2020-02-02T09:11:34+01:00
keywords: [privacy]
categories: [securityprivacy]
---
Two years ago I read George Orwell's novel 1984 that describes a dystopian future where authoritarian regime got complete control over people's lives. The book is amazingly written and it is as relevant today as it ever was. Reading it sent me on down a rabbit hole of research about how much privacy do I have in my current life. Let me put on my tinfoil hat and tell you some of what I found.

## What is privacy?

> **privacy** noun
> 
> the state of being alone and not watched or interrupted by other people

This is how the Oxford Dictionary defines privacy. For me, the word represents that if I don't wish my actions (I read an article about a controversial topic), data (my bathing suit pictures from latest holiday) or thoughts to be public (available to other people to see), they won't be.

## Why is it important?

People behave differently in private and when they know they can be watched. That's the whole point of surveillance cameras. When you talk to your friend, would you say the same things if you knew the transcript of your conversation is going to be available to your potential employer during the next interview? No, you would censor yourself and that would not only limit the quality of conversation but it would over the time limit the quality of your thinking. You wouldn't be able to discuss any controversial topics like addiction, racial issues, religion or politics.

Now half of the readers switched off thinking: "Tomas, you need to stop reading books like 1984". But to those I say: Just search for "social media background checks". And that's only one example.

## Most common argument against privacy

People who tend not to worry about their privacy often say a variation of "If you don't have nothing to hide, you have nothing to worry about".

Proponents of privacy usually chuck a wobbly at this point, yelling: "Yeah, yeah, give me keys from your house and all your papers like id's and bank account statements if you've got nothing to hide". And while this is a perfectly valid reaction to the naive statement, one argument won me over much more:

**The fact that you don't decide to exercise your right doesn't mean that other people don't need it or that the right shouldn't be present.**

You could just substitute privacy with any other right like "freedom of speech" or "right to organise".

I'm not an activist, oppressed minority and I live in a country where freedom of speech still works well. That doesn't mean I'm going to be sharing my private conversations/data just because nobody is probably interested (except for thousands of companies using the data for advertising). That would make it that much harder for people who have to hide what they are talking about in an interest in their safety.

## But what about terrorism/crime/???

> Those who would give up essential Liberty, to purchase a little temporary Safety, deserve neither Liberty nor Safety.
>
> Ben Franklin

Stronger or at least the current rights ensuring privacy are making it harder for law enforcement agencies to fight crime and terrorism, that's for certain. But it's not a black and white issue. There is no guarantee that if we give away part of our privacy there's going to be less crime and less terrorism. Just look at the NSA's scandals over the last decade and see if there are fewer mass shootings and less terrorist attacks in the states.

On top of that Terrorism is a massively exaggerated issue. Since 9-11, terrorists kill every year "about fifty people in the European Union, about ten people in the USA, about seven people in China, and up to 25,000 people globally (mostly in Iraq, Afghanistan, Pakistan, Nigeria and Syria). In contrast, each year traffic accidents kill about 80,000 Europeans, 40,000 Americans, 270,000 Chinese, and 1.25 million people altogether." [1]

How much of your privacy will you sacrifice for 0.06% improvement in traffic casualties?

## There's no such thing as free lunch

My closing thought is that a lot of services we are using are "for free". When you use Gmail, all your emails and hour behaviour (do you read the email, do you click through, do you respond) is being parsed and used for ad targeting. Same with Google search, Youtube and Facebook. It doesn't have to be like that, but you'll have to pay. In one of the next articles, I'll dive into alternatives to mainstream online services.


[1] - Yuval Noah Harari - 21 Lessons for the 21st Century