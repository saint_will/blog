---
title: "Meditation, introduction"
slug: meditation-introduction
date: 2020-02-16T10:37:43+01:00
keywords: [lifestyle, meditation]
categories: [lifestyle]
---

This post is an introduction to meditation. If you are meditating regularly, you can skip right over it.

## What I mean by meditation?

When I refer to meditation, I mean mindfulness meditation. That means paying close attention to what is happening in your own mind and consciously trying not to change or force anything. The main goal (at least for me) is to understand better what my own mind is and how it operates.

When I'm talking about meditation I'm not talking about religion. I'm also not trying to make you believe in some supernatural phenomena. So even though I found certain spiritual concepts from Buddhism useful, I'm going to leave that topic out completely since it's not necessary and it too often triggers "bullshit filters" and makes people zone out.

## Why am I doing meditation?

I had actually quite unusual reason to start meditating. I was doing online course on dating. James Marshall's [5 principles of natural seduction](https://vip.thenaturallifestyles.com/five-principles-secret)[1](#footnotes). And the first principle was "Awareness" with an introduction to meditation and mindfulness concepts. It was the single most valuable thing that researching and learning dating brought me.

Now I'm no longer focusing on meditation as a means to be able to better talk to strangers. Nowadays I've got two reasons I keep meditating.

The first one being the more immediate observable benefits. I'm no longer as affected/controlled by my emotions as I used to. When I get swept away with anger, anxiety or any other negative emotion, I can sometimes realize that it is just an emotion and I have a choice whether to keep feeding it and focusing on it or let it go. I can earlier realize that something is not making me happy. I also spend less time lost in thought oblivious to my surrounding. All of these increase greatly the quality of my life and happiness. Of course, I'm not a saint who never gets angry or depressed, these emotions are just not as strong as they used to.

The second reason I keep meditating is that I increasingly realize that I know nothing about the inner workings of my own mind. By quiet introspection I'm trying to find answers to questions like: What is a thought? What is vision and how is it different from hearing? If you haven't spent some time sitting quietly and observing the mess that we all have in our heads, this is bound to sound willy-nilly esoteric and I'm therefore going to stop right here.

## How do I do meditation?

The process and purpose of meditation sound simple: **Sit down with your back upright and observe what is happening in your mind**. If it only was this simple in practice. I'm trying to do exactly that but there is a plethora of little tricks and tips that are very useful to support the main objective. For example, I prefer to close my eyes even though I'm sometimes meditating with eyes open. When my mind is all over the place, I focus solely on my breath.

What I find indispensable are meditation courses and guided meditation and nowadays around 20% of my meditation time are guided meditations. I can, with clear heart, recommend the following two courses: [Headspace](https://www.headspace.com/) which I found excellent in the beginning. And [Waking Up](https://wakingup.com/) which provides much more in depth explanation of the concepts and I use it till this day.

And how long do I meditate? I'm striving for an hour a day but I can observe positive results even when I reduce this time to twenty minutes a day. Majority of the meditation teachers are saying that ten minutes a day is enough to observe positive benefits, but twenty seems to be the minimum amount of time for me.

## Conclusion

People sometimes compare meditation to working out. Working out makes your body healthier and meditation makes your mind healthier. This metaphor goes a long way:

- at the beginning, you don't want to do it, it's painful
- and even when you do it for months, there are times when you don't want to start
- you don't observe the benefits till you do it for a longer (month+) period of time
- it allows you to experience world differently
  - you can hike up a hill and take the view
  - you can listen to something that would usually insult your ego and not get angry or reactive
- the benefits go away as you stop
- the experience is not transferable - try to explain to someone the feeling you get when you finish workout/run/hike/climb
- doing it regularly is the only way to benefit from it (one workout in a month is probably worse than none, same with meditation)
- creating a regular time in schedule helps to keep it up
- *and so on…*

### Footnotes

- [1] This is actually the only course that I would recommend to anyone, because it doesn't treat women and dating as a game that you can either win or lose.
