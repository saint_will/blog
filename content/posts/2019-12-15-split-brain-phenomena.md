---
title: "Split brain phenomena"
slug: split-brain-phenomena
date: 2019-12-15T20:00:00+01:00
---
What do you do when epileptic seizures take a control of your brain and large storms of random neural activity starts to spread through your brain? You cut your brain in half to limit the blast radius.

The human brain is split into two hemispheres right above the brain stem. The reason for that is not clear but I'm pretty sure it's the same why we split CPU's into cores. Connecting the layers of neurons is expensive and probably not necessary. (all my speculations)

The independence of these two parts of the human brain became obvious in the research of Roger W. Sperry. He started studying interocular transfer with cats (studying why information learnt with one eye is available to the solving problems with the other eye). He found out that when severing the corpus collosum in cat brain, he can teach the cat to recognize geometric objects with one eye, but the learning won't transfer to the other hemisphere and the other eye can't recognise the objects.

This initial research informed Sperry's methodic for investigating patients who thanks to their intractable seizures had their corpus collosum severed in a procedure called callosotomy[^1]. He invented series of tests that were relying on showing different information to different eyes. Using these tests he was able to demonstrate what each hemisphere is better at (e.g. Left - language, arithmetic, analysis; Right - spacial and pattern recognition).

These tests convinced him that (in his words) each hemisphere is:

> indeed a conscious system in its own right, perceiving, thinking, remembering, reasoning, willing, and emoting, all at a characteristically human level, and ... both the left and the right hemisphere may be conscious simultaneously in different, even in mutually conflicting, mental experiences that run along in parallel.

He was awarded with Nobel Prize in Medicine for this discovery in 1981 [1](https://www.nobelprize.org/prizes/medicine/1981/sperry/article/)

[^1]: [95 patients in 20 years in Montreal](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2812715/) It seems that for certain types of seizures it is still a valid type of treatment today.
