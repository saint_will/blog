---
title: "The most boring technology Stack ever"
date: 2020-03-07T16:33:00+01:00
draft: true
---

1. Introduction - one paragraph explaining what this is all going to be about - leave for the end
1. Reasons why software projects fail
1. How many can be traced back to a technology?
1. Hypothesis
1. Describing the boring stack
1. Drawbacks
   1. Hiring, talent retention, cost of lower efficiency (but that might be outweighted tenfold)
1. Examples of the boring stack
1. 


When looking at the projects that I worked on in the past, I started seeing lifecycle that is very common in the software development. You start a new project. The technologies are cool and interesting. You try a lot of new things and at the beginning everything is well. But after a while bad code starts to accumulate. Making new changes becomes harder and harder and in the end even the technologies that the software runs on become very outdated.

I've seen this so many times that I wonder whether that's the only way. Of course, when you read books like Refactoring from Martin Fowler, you are reassured that with good practices, there is an alternative. But I never quite saw it happen. Except maybe for one insurance software.

The question came up in my mind. Is there something I can do to increase longevity of a software project apart from a good software engineering practices and discipline which is not something to be easily affected or changed. My hypothesis is that choosing the right technologies can significantly increase the chance that the project will be maintainable in a distant future.

> Hypothesis: Using technologies and frameworks that have been used for over 20 years and are still active and successful will significantly and positively impact the lifespan of your software.

