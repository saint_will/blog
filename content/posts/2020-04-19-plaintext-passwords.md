---
title: "Using text passwords for encryption is safe enough"
slug: plaintext-passwords
date: 2020-04-19
written: 2020-04-18
keywords: [security]
categories: [securityprivacy]
summary: Is my Keepass key chain safe if I only use text password to protect it? Yes it is.
---

I had a creeping suspicion in my head lately. I'm using a text password as a key to encrypt all my other passwords. I started to worry that the text password is not safe enough and I'm only placing a minor obstacle between my passwords and a curious person. Let's have a look whether I was right.

I like the idea of knowing that some things are just mine, especially my thoughts. That's why I like to have as much of my data encrypted or, when 3rd party service stores it, at least behind a strong password. I recently wrote about security and about your [password manager being at the core of it](/posts/security-is-like-ogres/#the-core). My [password manager](https://keepassxc.org) contains all my debit cards, all passwords to websites, and a quite few encryption keys. But there is one issue with my password manager that was my little dirty secret. Until now.

## Using text passwords

My keychain file (the file where password manager encrypts all the passwords) is using only a text password as an encryption key and I store this keychain on Dropbox (which is relatively unsafe storage). I did this because I was always afraid that if I use a long key I don't personally remember (e.g. RSA private key), I'm going to lose it and all my data with it. The password is not trivial, but it's only 15 characters. I now want to look at how safe my data behind this password actually is if I need to consider any alternatives to a text password.

I have to admit that for designing the password I was using a [XKCD comic](https://xkcd.com/936/). The advice there is the following: Choose 4 random words, chain them together and the level of entropy is going to be ~ 44 bits which means that at 1000 guesses/second it's going to take 550 years to crack.

The problem with my keychain is that it's not a web service that can limit the number of login attempts. If someone gets a hold of the keychain file, the sky's the limit when it comes to brute forcing the password.

### Cracking my own password

- Password Manager: [KeePassXC](https://keepassxc.org)
- Cipher: AES (kdbx 4.0)
- Attempts per second: 1
- How many attempts are necessary 2^44

After researching several articles [[a]](https://madcityhacker.com/2018/11/04/cracking-keepass-databases-with-hashcat/)[[b]](https://www.rubydevices.com.au/blog/how-to-hack-keepass)[[c]](https://keepassxc.org/docs/#faq-security-why-pm)[[d]](https://www.harmj0y.net/blog/redteaming/a-case-study-in-attacking-keepass/)[[e]](https://security.stackexchange.com/a/8477) I learned that my assumption from the previous section is incorrect.

> The problem with my keychain is that it's not a web service that can limit the number of login attempts. If someone gets a hold of the keychain file, the sky's the limit when it comes to brute forcing the password.

The keychain file is encrypted using a cypher (AES) that by itself *could* be easier to crack. However, before the key is used it goes through a large number of so called key transformations[^1] so in the end it takes milliseconds to validate one password. On top of that, you can tweak how many times the key should be transformed based on how long you are willing to wait for the keychain to open.

So when you saw "Attempts per second" being equal to 1, that means that I asked KeePassXC to run the key transformations for one second on my i9 MacBook Pro.

That means that if I tried to brute force (with a dictionary) a password with 4 common words in it on my laptop, it would take 550,000 years[^2].

Now, this number is definitely way too optimistic. Following factors could significantly reduce the time:

1. I'm not sure whether KeePassXC uses all 12 CPU cores on my PC, probably not. That means I could divide the brute force time by 12 - 46,000 years
2. Using some strong GPU could easily bring down the number by two orders of magnitude - 460 years
3. Using 460 strong computers would bring this time down to year

Even though these steps would require a lot of effort, they are not outside of the realm of possibility. It would just cost up to 12 million dollars[^3].

*These computations are very crude. Please if you have better understanding/information about the kdbx 4.0 file format and how to brute force the passwords, let me know*

## How to prevent a hacker with $12M from getting to your passwords?

It sounds like there is no need for a drastic change. Few orders of magnitude will make the change infeasible even for Jeff Bezos.

**Add a couple words that are not in the dictionary. Use a different language or a slang. That should rule out the dictionary attack and make the password not crackable at least in your lifetime.**

This investigation was a very pleasant surprise to me. I was living under an impression that I'm storing my passwords pretty much in a glorified text file and now after invalidating my assumption I realize that my passwords are safe.[^4]

Six-word password including some non-dictionary words is in combination with thousands of key transformations almost impossible to crack.

#### Acknowledgements

- Thanks Andrei Damian-Fekete for helping to clarify parts of the article and fixing some of my grammar

[^1]: 60,000 times by default
[^2]: I'm still using the [XKCD comic](https://xkcd.com/936/) as a base for my calculations
[^3]: 1 `p3.2xlarge` [costs $3/hour](https://aws.amazon.com/ec2/pricing/on-demand/) => 460 * 3 * 24 * 365 = 12,088,800
[^4]: All this effort can be reduced to almost no effort at all if someone can get access to your computer and listen on your keyboard the moment you are typing in your password. The same is true if there is a bug in the keychain software itself. For example the KeePassXC [can't guarantee there isn't an encryption bug](https://keepassxc.org/docs/#faq-audit). However, the same applies for other encryption mechanisms like OpenSSL that encrypts virtually all traffic on the internet.
