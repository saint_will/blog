---
title: "How I stopped drinking for a year"
date: 2020-01-13T20:00:00+01:00
keywords: [lifestyle]
slug: alcohol-abstinence
categories: [lifestyle]
---
> As all Buddhas refrained from alcohol until the end of their lives, so I too will refrain from alcohol until the end of my life.

The five precepts, the most important system of morality for Buddhist lay people [1](https://en.wikipedia.org/wiki/Five_precepts#Fifth_precept)

I'm writing this article on Saturday morning after a Friday night out with my Friends. We've hit the pub and everybody was merry. Beer was flowing and shots were had, just not by me. I came back home around midnight. Brushed my teeth and set an alarm for 8AM so I get 8 hours of sleep. In the morning I did some journaling, meditated and after nice breakfast and coffee sat to my computer to write this post. No hungover, no regrets, clear mind.

This introduction would look radically different a few years back. I would have drunk beers and shots, tried to convince my friends to take the party further and if successful, I would rock up home at 4+AM, wallet empty and I wouldn't even remember it. In the morning I would just grab my laptop and watch TV series for a good part of the day feeling sorry for myself.

## My history of drinking

I started drinking very late for a Czech guy. I had my first serious piss up on the day of my 18th birthday. The same night I had my first black out as well. Experience that would accompany me on each larger social occasion till my 30's. Since then my life revolved around alcohol. Every week there would be several occasions to drink. Climbing? What about first climbing and then drinking in a pub? Walking on Slackline? How about get a few bottles of wine to make it more fun.

Don't let me start with uni. There was a pub under almost every apartment building in my dorms. Student parties happening at least every fortnight. And finishing exams? I don't know many better reasons for celebration (childbirths, weddings, finishing uni).

This is not unusual at all. My experience would be of an average Czech adolescent.

One returning theme would be part of my drinking experiences. I'm a better person when I'm hammered. I like others more, I'm more fun, I'm not afraid, I'm more extroverted. I'm more everything. Quite often when I wasn't enough of something, solution was just a few drinks away.

## 12 years later

Not that much changed in regard to drinking when I was approaching my 30th birthday. I knew a few things. I didn't feel that much shame or anxiety after my blackouts, I knew to drink a lot of water as I'm out drinking (something they should have thought us in school), I had better taste in spirits, wine and beer.

I quit my job as well, got a decent bonus and had good savings and decided to travel for two years. Europe, Asia, South America. Without even knowing it this was the beginning of end (for my drinking).

After 5 months of travel and experiences I found myself in a [Buddhist monastery in Southern Thailand](http://www.suanmokkh-idh.org/). It was a day before New Years Eve and just a few days after participating in one of the infamous Full Moon parties of Ko Phangan (the Ibiza of Asia). I stayed in the monastery for 12 days on a silent retreat. I didn't get any huge revelations or awakening but on day 8 our retreat leader had a talk about the 5 precepts, analogous to the Christian 10 commandments.

The lady was talking about not hurting people and not stealing and then she mentioned the fifth. She said "And the fifth precept huh?" and smiled. There were a few murmured laughs around. Then she said something that stayed with me for the rest of the retreat and I sure had a lot of time to think about it.

> Without the fifth percept, you can't uphold the rest.

Reflecting on my past, majority of the things I've done I'm not proud of were under the influence of alcohol. I did mental balance scorecard and decided to try a year without alcohol. Might as well since the retreat finished on 11th of January, only 354 days to go.

## The alcohol free experience

The experience is very daunting, after using alcohol as social lubricant for over a decade, it's not easy to socialize. You realize that the interactions with strangers are more awkward and that you'd actually much prefer to go home and read/sleep/do the thing. I'm not saying that I would *actually* prefer reading over human contact [1](#note1) but I surely feel that tendency more when I'm sober.

One thing you notice almost instantly is the weird experience of not having a hungover after a night out. Hungovers were a part of my life, big one too. The only thing I have to plan for now when I'm coming home from a party is to shift my alarm to allow for roughly eight hours of sleep. I don't have to worry about spending a good part of the next day in my bead watching movies/series.

### Dating

This is, where I think a major hole in an black and white argument like "Drinking is terrible, everybody should stop" breaks down. Of course it's possible to start a romantic relationship without an alcohol, but a small dose will definitely help and most of us in this day and age need every bit of help they can get. Alcohol makes people putting aside their reservations and judgement and makes romantic encounters easier to happen.

Since I had a girlfriend at the time I stopped drinking, this issue is not affecting me, but I would probably recommend to single people to keep drinking.

## The future

It's seventh of January , year and a week since I stopped drinking. I'm not planning on starting any time soon because I don't trust myself with being able to keep a moderate approach to alcohol. And to be honest, I don't miss that experience at all after a year. I've learnt how to enjoy the company of my friends and family without alcohol and I think this reality is going to be like a good whisky, getting better with age.

<span id="#note1">[1]</span> Or at least not all the time.
