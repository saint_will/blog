---
title: "How to describe a software issue"
slug: describe-software-issue
date: 2020-09-13
keywords: [software, open-source, issue, bug]
categories: [techsoftware]
summary: Become that one person who gets their technical problems solved first.
---

This article will help you become that one person who gets their technical problems solved first. Keep following these three guidelines, and you will greatly reduce the amount of suffering in the world.

{{% theme-image width="450px" filename="describe-software-issue.svg" %}}

Imagine that you found an issue in your favourite software, here are the steps that you can take to report the issue to a software maintainer. They don't take much effort, but they'll make a world of difference for the person trying to understand your report.

### Check existing issues

Majority of the software I use has an open issue tracking system. Before you start writing about the problem you are facing, check the issue list. Try to search for a few terms that would match your issue. Chances are the issue is already documented.

If you manage to find an existing issue, leave a comment saying the same thing happens to you. If some important information about the issue is missing, mention that as well and possibly follow the following two guidelines.

This reduces the load on both maintainers and you. Maintainers don't have to classify the duplicate issue, and you don't have to put in the effort to describe the issue clearly.

Examples of issue tracking:

- [Mozilla's Bugzilla](https://bugzilla.mozilla.org/home)
- [GitLab issues](https://gitlab.com/gitlab-org/gitlab/-/issues)
- [GitHub issues](https://github.com/viktomas/godu/issues)

### Reproducing the issue

You might have heard about Rubber duck debugging[^2]. In software engineering, it means you try to explain your problem to a rubber duck and by doing so, you'll understand the problem better and often solve it altogether.

Describing your issue works similarly. If you can't clearly explain how did you arrive at the issue, what makes you think that an engineer without a context can? If you can, mention all the custom settings, software version and attach any logs that could contain cues for the engineers.

### GIF of the problem

GIFs are not only a great way to thank you with Tom Hanks video[^3], they are also a fantastic tool to show the issue. They say that "A picture is worth a thousand words"[^1]. Moving pictures increases the worth even further. If the issue manifests itself with any user interface problem, or if you can reproduce it visually (e.g. click here, type this, click on the submit button), then GIF is the perfect choice for showing the issue.

- [Kap](https://getkap.co/) - Mac screen capture
- [Peek](https://github.com/phw/peek) - Linux screen capture

#### Examples

- [Issue recording incorrect behaviour of the GitLab editor](https://gitlab.com/gitlab-org/gitlab/-/issues/223704#relevant-logs-andor-screenshots) (Web IDE)
- [A recent issue to implement inserting snippets in VS Code](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/issues/129)

---

Following these steps will save time for the maintainers. They can spend more time on making the software  better for you.

But more importantly, **it will save you time**. If you only send out an email saying something like "messaging doesn't work", you are in for a whole lot of trouble. Get ready for the back and forth communication when the engineer tries to extract enough details out of you to reproduce the issue. That's in the end much more effort compared to a cleaner first report.

[^1]: https://en.wikipedia.org/wiki/A_picture_is_worth_a_thousand_words
[^2]: https://en.wikipedia.org/wiki/Rubber_duck_debugging
[^3]: https://media3.giphy.com/media/AhJIPUnoUYgyA/giphy.gif
