---
title: "Why is email so hard? Unsatisfactory migration from Gmail to Zoho"
slug: why-is-email-so-hard
date: 2020-10-25
keywords: [google, gmail, zoho, email, privacy]
categories: [techsoftware]
summary: I don't want Google to own my digital life, but it's hard to find alternative with good user experience.
---

I've just tried to get off Gmail. After a long twelve years, I decided it's time to do what I preach and remove one more dependency on Google. I've already done that with [YouTube](/posts/break-free-from-youtube/),

**TL;DR**: Zoho Mail as an email provider, Thunderbird and K-9 as clients. But it is a pain.

{{% theme-image width="350px" filename="why-is-email-so-hard.svg" %}}

I was sick of sharing my data with Google. I was sick of having a username that I made up in my late teens (still better than the previous two: `little.pink.hood` and `DJ.bacon`). And I was sick of all the rubbish that ends up in my Gmail inbox. It was time to start from scratch.

Oh boy, was that a naive idea. Did I really think that I can easily change my email to a more generic service?

I severely underestimated how terrible the user experience is as soon as I stop using a custom-built software like Gmail or Office 365. These providers built their clients to work well with their custom email services. Trying to connect to the email server using IMAP feels like travelling back in time.

The latest version of IMAP, IMAP4, has been published in [2003 - 17 years ago](https://tools.ietf.org/html/rfc3501). Two years after Microsoft released Windows XP. No wonder that people only use the web and native clients of the few largest email services.

## Choosing the provider

I wanted my email provider to be secure, always running, and cheap. I didn't want it to sell my personal data or me as a subject for advertising.

I considered using [sovereign](https://github.com/sovereign/sovereign). It is a set of scripts for installing your custom email server. But do I really want to be responsible for keeping my email up 24/7? I could foresee the Christmas or Summer holiday outage of my email server.

Recently, there was a big hype around hey.com, but I'm not ready to pay $100 annual fee to still have an email address like `my-name@someone-elses-domain.com`. When they start supporting custom domains, I might switch if I'm still not used to the UX of third-party IMAP email clients.

So I had to start looking for paid services that are cheap, but they provide IMAP and SMTP connection. This connection is needed to use the email server with any email client.

I got [Zoho Mail Lite](https://www.zoho.com/mail/zohomail-pricing.html). It cost 14 EUR a year, and it allows custom domains. That means I can finally have `me@viktomas.com`. I used the free version of Zoho before, but that didn't allow me to use IMAP (brilliant pricing move on Zoho's part). Upgrading Zoho was a clear choice.

## Choosing a client, either rock or a hard place

I use macOS. It's an ok system, but I wouldn't use it if it wasn't required for work. One day I'm going to move back to Linux. The only way to keep this option open is not to lock in something as critical as email to a platform (e.g. starting using the osx Mail app). For that reason, I strictly picked [open-source](/posts/foss/), cross-platform client for desktop.

[Thunderbird](https://www.thunderbird.net/) seems to be the most supported desktop client out there. Using Thunderbird after being used to Gmail is not great. I haven't had to do this much internet searching and configuring after installing a desktop app in ages. I had to configure things like DPI for the app, so the UI isn't so tiny on a retina screen. Email threads are still not working 100%. Then again, the Thunderbird team is not making money off me and really appreciate the work they've done.

For mobile (Android), I found [K-9](https://github.com/k9mail/k-9). Unfortunately, it's not being released through Play Store any more, and I had to get the latest through [F-Droid](https://f-droid.org/packages/com.fsck.k9/). So far, it seems to be working well. It can't match the UX of native apps like Gmail, but I don't expect it to. The K-9 OSS team is doing a fantastic job.

I've literally just finished the transition. I'm now planning on slowly setting the new email to all my accounts. I've still got 12 years worth of emails in Gmail, it's 2.4 GB [^1]. When I'm ready to only use my Gmail account for forwarding, I'm going to migrate those emails to Zoho. Please, if you use a different solution and you are happy, send me a message, you know my email.

[^1]: I reduced the size by 30% by removing emails from the following Gmail searches: `category:social  -L:important`, `category:promotions`, and `has:attachment larger:1M`.
