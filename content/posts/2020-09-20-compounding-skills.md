---
title: "Compounding skills improve your life exponentially"
slug: compounding-skills
date: 2020-09-20
keywords: []
categories: [productivity]
summary: Learn note-taking, writing, or body-friendly exercising today and profit for the rest of your life.
---

We have an intuition about the benefit we get from learning a skill. Learning how to remember information better is more beneficial than learning how to recognise trees using leaves. What is the cause of the greater benefit? And what are some examples of more and less beneficial skills?

{{% theme-image width="500px" filename="compounding-skills.svg" %}}

I determine the utility of a skill as a sum of how much fun it is, how valuable it is to other people and how large is the context in which I can apply the skill. The best skill to have is the one that is fun to do, useful to people, and applicable in a wide area of activities.

Today, I want to focus on the last part of the equation, the skill's context size. The context size is what creates the compounding effect of the skill.

For example, if you are an athlete, you can learn how to kick a soccer penalty. This skill is useful for a soccer player, but it has got a limited range of applications. On the other hand, you can learn general anatomy: how muscles work, how to exercise them and stretch them. The second skill will help with soccer training, but it will also help with any other sport as well as with staying healthy and mobile.

Here are three examples of skills that have a large context area and so they are going to be useful throughout a variety of activities. If you learn them, they'll be compounding their benefit for years to come. The sooner you learn them, the more benefit you'll reap. Same as financial savings.

## Note-taking

I've spent a good portion of this year learning how to take notes. I discovered [Zettelkasten note-taking](/posts/slip-box/), and I realised the compounding potential.

When you make notes using the Zettelkasten method, you optimise for read[^1]. You spend extra time writing the note as if the reader (the future you) doesn't have any context, which makes all the notes timeless. You can read them ten days or ten years from now, and you should still get the same understanding. Every new note increases your knowledge base forever - a perfect example of compounding.

## Writing

Being able to clearly express your thoughts is a skill that permeates every knowledge work. Learning how to write so that people understand you is a skill that will stay with you at every step of your life.

I decided to learn how to write this year. It is now more important than ever with my [remote job](/posts/remote/) and with the increasing recognition of how remote work can be more productive and more healthy for knowledge workers.

One of the paradigm shifts in remote work is writing everything down. Then other people can search through internal wiki rather than tapping you on the shoulder, which saves your time. But this can only work if they understand what you wrote.

## Body-friendly exercise

The best people to teach you how to exercise are physiotherapists. They usually help people to recover from injuries or to mitigate some of the damage we do to our bodies with sitting jobs. They also know well what exercises are going to hurt you (e.g. poorly done crunches) and how to alter them so you won't hurt your back or joints.

If you learn how to exercise to strengthen your core, centre your joints, and stretch shortened muscles you are not only going to better perform in sports, but you are going to stay healthy and mobile for much longer in life.

## There's plenty more

Financial investment, communication skills, meditation, critical thinking and more. That's a brief list of what is now going through my head. I'm organising my thoughts on what to focus on next. If you've got any examples or thoughts on the topic, let me know on twitter or send me an email.

[^1]: In programming, that means optimising the system for users retrieving information from it, usually at the cost of write operations.
