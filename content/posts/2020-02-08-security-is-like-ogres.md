---
title: "Security is like ogres"
slug: security-is-like-ogres
date: 2020-02-08T09:17:48+01:00
keywords: [security]
categories: [securityprivacy]
---

I've just seen security report in the news about [a critical Bluetooth Vulnerability on Android devices](https://insinuator.net/2020/02/critical-bluetooth-vulnerability-in-android-cve-2020-0022/). The report says that the attacker was able to run arbitrary code on Android 8 and 9 devices. Just by being within the Bluetooth range. That got me thinking. How safe can I be from breaches of my privacy/security. Ever. No matter how much effort I would spend on keeping my devices and accounts secured.

There is going to be kind of onion-layered model to the security I've got in mind. The title of this article is referring to the famous line from Shrek, (Ogres are like an onion). The metaphor will take us a long way. Size of each layer represents three things: the **amount of our private data** that leaks, the **probability** that it leaks and **how ok we are** with leaking (e.g the onion skin - large amount of data about us leaks, it's happening as we speak, and we are mostly ok with that). The closer to the core we'll get the more important the information is to us and the more close to the heart we'd like to keep it.

## The Skin

The skin is the membrane that protects the onion. It is the largest surface area and by definition it's exposed to the outer world. In our digital life, skin are the services that we use every day - social media, email, chat applications. We share information with those services (most importantly information about our behaviour).

The size of this layer means that the amount of data that leaks is massive. We are almost willingly sharing our location, behaviour, habits and detailed personal information (are we single, how much do we earn..) (we don't share this information directly, but our behaviour falls into certain categories) with thousands of companies, and they use that data for advertising.

The probability that this is happening is ~1. It's happening as we speak.

The only thing we can do about this is to choose the services we use and be more careful about the data we provide them with. This is no easy, black & white, problem with easy solution like: install XYZ and live happily ever after. Google provides the best search results, all your friends are on Facebook and that Insta feed is so nice. There are privacy oriented alternatives at [privacytools.io](https://www.privacytools.io/) but using them usually comes at the cost of "using something different than the majority of the population"

## The inner layers

The inner layers are the part where you can make the most difference. They are not exposed as the onion skin, they are juicier and you wouldn't be ok with that information falling into someone's hands. This includes your private emails, messages and cloud files. We are no longer talking about behaviour (e.g. google knowing that you like to shop on eBay for things costing $100 each month). This is more about the specifics. This is some particular person knowing that one of those eBay items was a big black dildo.

The probability of the inner layers being exposed is not too high. And you can make it tiny by using the best security practices like password manager setting different password for each site. Use Two-Factor Authentication for you most important services. Use single sing on with Google or other provider and so on. I'll be going into details of these in future articles but for now simply Googling "Personal cyber security" will find satisfying results.

## The core

This is where the juiciest stuff it's your password manager itself. It's your online banking. It's all the files stored on your encrypted hard drive. You can tell it's not too much data compared to the amount of your Instagram pictures or instant messaging with your closest friends.

You would certainly not expect this data to leak ever and luckily the chance of that happening is very low. The things responsible for keeping this layer so hidden are the core technologies of the internet, SSL, encryption of all kind and security of your operating system. These things are used by all the people (exaggeration) and so their security is extremely tight. That being said, breaches like the one mentioned in the security report mentioned at the begging of this post still happen.

There is only one thing we can do here, making sure that we use the best technologies available (always HTTPS, encrypting hard drives, use secure OS like mac or Linux) and mainly **installing security updates as soon as possible**. There are always going to be vulnerabilities, but we can reduce how long they are exploitable by running frequent updates.
