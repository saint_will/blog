---
title: "Getting Things Done implemented in 2020"
slug: gtd-implementation
date: 2020-07-18
keywords: []
categories: [productivity]
draft: true
---



## Workflow example

GTD book was published in 2001, the year that Windows XP was released. You could buy a first iPhone 6 years later. This means that the book focuses disproportionate attention to physical organising of paper. Whole chapters are dedicated to choosing right office supplies.

I personally scan every paper using the Dropbox mobile app and then everything is just information that I handle on my computer. The workflow example uses only three pieces of software:

- Google Calendar
- [Joplin](https://joplinapp.org/)
- [Google Keep](https://keep.google.com/)

I always used Google calendar and my company uses Google calendar so it was an easy choice, but any other calendar app will work for you as long as you check it daily and you receive notifications.

> Pro tip: If you have work and private Google calendar, [give them access to each other](https://support.google.com/calendar/answer/37082?hl=en#). That way you can see complete picture and crate and update events from anywhere.

The more controversial choice is Joplin App. My hard requirement was that I will be in full control of my data (Joplin stores data in local SQL Lite) which rules out cooler kids on the block like Evernote and Notable. I also wanted to write my notes in Markdown. Usually I would go for text files in my favourite text editor (VS Code), but I needed to have an access to my next actions when I'm not on my computer and so I needed a mobile app. Joplin does provide a mobile app and it synchronizes through Dropbox or other cloud storage providers.

Any note taking app that allows you to link between notes should work just fine for you.

### Stuff

[Stuff](#next-action-note-format) comes into my life from all directions and at all times. If I can't spare the few moments to correctly place the stuff into my Joplin system, it will have to stay as stuff for a bit longer. That's what I'm using Google Keep for. This app is very simple to use and you can add all kinds of textual and visual material in it. Anything that goes into Keep will stay there no longer than a day. I'll process it to the next action or a reference and put it in Joplin.

### My notes structure

I'm going to generalize the naming. Joplin uses Notebooks that contain Notes, but I'm going to be talking about Folders that contain Files.

At the root of my system are two folders:

- `active` - all next actions and other notes that will require more activity on them
- `reference` - material that I might want to query in the future for reference, but there's no action required on it any more

These are **distinct groups**, either I need to do something about the information or not.

The `reference` folder is not that important, the more you optimize it, the easier it's going to be for you to put data in it and to find them later. The GTD book offers some guidelines for the structure. The main value of the GTD method is in the `active` folder however.

### The `active` folder

The `active` folder contains one important folder `projects`. Most of the projects that I currenly work on would have their own file in there.

There are the following files in my `active` folder:

```md
# Next actions

- [ ] Call my friend Jim and ask him for contact information
      on the mechanic he was so happy with.(5min)

## Side projects

- [ ] Write initial draft of the "Practical GTD" article.(1h)

## Finance

- [ ] Go to [justetf](https://www.justetf.com/) and find
      VTI equivalent on Xetera exchange.(20min)
```

```md
# Project list

- [Blog](:/23a8fc6f48bd4ab185fc06057391223f)
- [Timer](:/39c1661b25c744c78a57a3ae4e1a5b3e)
- [Slipbox extension](:/2b5ef261047e4fc98c4f9b07b96683ef)
- [Analytics](:/dd823bed0406441ca71c5453421f0f78)
```

```md
# Waiting for

<!-- Don't forget to add timestamp when you add a line -->

- 3 days of nice weather to fix the van roof `2020-07-09 09:05`
```

```md
# Shopping list

<!-- contains items that I need to manually buy when I'm out of home -->
<!-- all your context-specific next action lists would be like this one -->

- [ ] SR920SW and SR927W watch batteries
```

(the links are references to other nots in Joplin)

Every action that gets checked off goes into the following file in my `references` folder:

```md
# Done

## 2020-07-14

- [x] Find out what exchanges is Degiro trading on, can I add NY? (10min)
- [x] Send bazillion to Degiro (5min)

## 2020-07-13

- [x] find out when my 2 months of DigitalOcean expire and put it in my calendar
```
