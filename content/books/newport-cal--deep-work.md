---
title: "Cal Newport - Deep Work: Rules for Focused Success in a Distracted World"
date: 2020-04-18
started: 2020-04-07
goodreads: https://www.goodreads.com/book/show/25744928-deep-work
shelves:
  - non-fiction
  - self-help
---

I have a mixed feeling about this book. On one hand it was extremely repetitive and the first part of the book felt like a broken record. But this might just be because I'm familiar with the concepts explained, and I was already putting a large emphasis on focused work free from distraction. When the topic is new to me I actually quite appreciate repetition since it gives me a lot of examples and points of view for the same argument.

The second part of the book, the part where you get concrete advice on what to do, was less repetitive. It fit well into the framework explained and there were a few gems that are going to be super useful to me:

- You don't have to avoid distraction (e.g. YouTube) completely, it's enough to schedule it well in advance
- The benefit of every tool and platform you use needs to be weighted against your personal goals and values so you can see the costs as well
- There is not clear cause -> effect relationship in a lot of the knowledge work (emails, meetings, ...) and in the absence of this clear measurement people default to what is easiest and often try to look busy doing it

Since this is such a short read, I would recommend it to a friend, especially if they are struggling with focus.

*Note: I wish the author added the citations to the text, so I could recognize when he's expressing his opinion and when is the fact supported by some research*
