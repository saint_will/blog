---
title: Douglas E. Harding - On Having No Head
fullTitle: "On Having No Head: Zen and the Rediscovery of the Obvious" 
date: 2020-08-17
started: 2020-08-12
goodreads: https://www.goodreads.com/book/show/817501.On_Having_No_Head
shelves:
  - non-fiction
  - spirituality
---

I've heard about Douglas' book when I read [Waking Up from Sam Harris](https://www.goodreads.com/book/show/18774981-waking-up). On Having No Head is a spiritual book, first published in 1961. Giving a good pointer to what Buddha might have meant when he said that there is no self.

The simple idea that Douglas presents in his book is this: "Can you see your head? If not, how do you know it exists?"

It can feel quite strange when you explore that experience and do the exercises.

For more information, I strongly recommend visiting [The Headless Way](https://headless.org). You can even participate in online meditation sessions. I've done it, and the community is welcoming and pleasant.
