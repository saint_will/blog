---
title: "Terry Pratchett - Guards! Guards!"
date: 2020-07-03
started: 2020-06-04
goodreads: https://www.goodreads.com/book/show/233679.Guards_Guards_
shelves:
  - fiction
  - fantasy
  - comedy
---

This is the fantasy series I was looking for. After reading this book, I'm using the [Discworld reading guide](https://i.stack.imgur.com/339aO.jpg) to binge read all the watch novels.

This book is following the story of a police unit in a city where crime is legalized and encouraged. This way, you run into hilarious situations like being persecuted for arresting the head thief or having troubles for suggesting that assassins are murderers.

If that wasn't funny enough, you've got the constable Carrot, two-meter tall man who was raised by dwarfs and considers himself being one. He sometimes goes into the cellar and bashes himself in the head with an axe handle to remind himself of the good times in the mine.

Overall, I haven't had this many laughing out loud moments with a book since reading [The Rosie Project](https://www.goodreads.com/book/show/16181775-the-rosie-project). I started reading Men At Arms - so far, I'm not disappointed.
