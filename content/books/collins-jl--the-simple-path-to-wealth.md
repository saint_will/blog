---
title: "J.L. Collins - The simple path to wealth"
date: 2021-04-14
started: 2021-04-11
openlibrary: https://openlibrary.org/works/OL19747351W/The_simple_path_to_wealth
summary: Find out how to retire early and think about investing no more than a few hours a year. This book offers a truly simple method for becoming financially independent.
shelves:
  - non-fiction
  - finance
---

Find out how to retire early and think about investing no more than a few hours a year. This book offers a truly simple method for becoming financially independent.

J.L. Collins' method fits in one sentence: "Learn to live way under your means and invest all your excess money into all market Vanguard indexed fund". It sounds to be too good to be true.

I'm not saying it is too good to be true, only that it sounds that way. If you invest tens of thousands of dollars a year, you want a complex and sophisticated method. The author says that investors are their own worst enemies: they overcomplicate their investment, try to pick the winning stock, and try to time the market. I'm guilty of all those things. After a few years of investing, I see that the author's advice is the only one I need. I've tried to time the market and pick an individual stock. Luckily[^1], I've always lost money on it.

I've read the Random Walk Down Wall Street from Burton Malkiel (the 2019 edition), and so I knew the concepts behind index funds, but I've learnt a few new things:

- Dollar-cost averaging is a bad idea. Historically, only one in four years the market drops. if you are trying to invest a big sum of money over time (say a year), you have a 75% chance you will be losing money. *(this is a very brief summary, have a look into the book for detailed explanation)*
- Buying your property is most likely not an investment but a splurge. Run the numbers. Don't assume it's always a good investment.
- If you can spend only 4% of your portfolio in a year, you are financially independent (you can live from your portfolio). The book mentions a study that was averaging a large amount of investing scenarios. A 4% yearly withdrawal from the investment was enough to keep the portfolio at least the same size over time in most of the scenarios.
- I've always worried about the future of indexed funds. Are the markets still going to be efficient if the majority of the capital is passive? J.L. Collins guesses that when there's most of the capital in indexed funds, there will be more gains from active investing, which will motivate more investors into active trading. The markets are going to balance themselves again.

If you don't have time to read the whole book, watch this [interview with J.L. Collins](https://www.youtube.com/watch?v=T71ibcZAX3I). I wholeheartedly recommend reading this fantastic book.

[^1]: Luckily, because it allowed me to fool-proof my investment strategy early.
