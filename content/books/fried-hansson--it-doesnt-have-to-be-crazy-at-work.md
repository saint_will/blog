---
title: Fried, Hansson - It Doesn't Have to Be Crazy at Work
fullTitle: It Doesn't Have to Be Crazy at Work
date: 2020-07-19
started: 2020-07-16
goodreads: https://www.goodreads.com/book/show/38900866-it-doesn-t-have-to-be-crazy-at-work
shelves:
  - non-fiction
  - business
---

This is the third book from the duo Jason Fried and David Heinemeier Hansson. Rework was about product design, Remote was about remote work, this one is about company culture and processes to support deep work.

I am fortunate enough to work at GitLab where we implemented the majority of the processes described in this book ([GitLab Handbook](https://about.gitlab.com/handbook/)). But few concepts were exciting, and they applied to my personal life and side projects.

**Prioritisation is the best productivity method.** You can optimise how many things can you do during a day and how well, but nothing gives you as much efficiency as **not** doing something. Prioritisation helps you not to optimise work that doesn't have to happen at all.

**Optimising workspace for deep work.** GitLab is great at this, and the book gave me a vocabulary to talk about this phenomena. You want to put respect for other people's time and attention at the forefront of your company's processes. Examples: People can't see each other calendars, so it makes it hard to organise meetings. No decisions should happen in real-time chat, so you don't have to be always online or risk missing out.

**Budgeting vs Estimation.** People are bad at estimating, but good at budgeting. Ask them how long feature X is going to take, and you won't get a precise answer, but tell them to get as close to feature X as you can in 6 weeks (budgeting), and they will do a good job.
