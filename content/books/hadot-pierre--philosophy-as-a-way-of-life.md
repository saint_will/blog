---
title: "Pierre Hadot - Philosophy as a Way of Life"
date: 2020-11-16
started: 2020-10-27
goodreads: https://www.goodreads.com/book/show/305860.Philosophy_as_a_Way_of_Life
shelves:
  - non-fiction
  - philosophy
  - biography
---

[Pierre Hadot](https://en.wikipedia.org/wiki/Pierre_Hadot) was a French philosopher who spent his whole life studying ancient philosophy. He was a key proponent of thought that ancient philosophy was not the theoretical discipline studied at universities as we see it now but a way of life. Everyday practical exercises were an inseparable part of philosophy.

Through the highly practical point of view, Hadot explains the work of Socrates and Marcus Aurelius. Modern scholars often misinterpreted Marcus Aurelius as a pessimist. In his Meditations, Marcus talks very dispassionately about sex, food and power.

> And in sexual intercourse that it is no more than the friction of a membrane and a spurt of mucus ejected.

But instead of being a pessimist, he just tried to objectively describe things that people assign large value to and in doing so crave those things less.

This book made it clear that philosophy was a great discipline of spirituality and only with the rise of Christianity, the philosophy became the study of logic and morality, disconnected from everyday life. Religion replaced the parts of philosophy that used to govern people's lives in ancient Greece and Rome. And Hadot puts forward good arguments for Christianity copying and extending the good parts of ancient philosophy.

Even though this book is a heavy textbook, it gave me an insight into the philosophy I'd love. The philosophy that's all about bettering yourself and about living a full life instead of having arguments about fictional scenarios. Also, ancient philosophy seems to be close to Buddhism in all key aspects:

- Desire and aversion are causing all suffering. The moment you want something you don't have, or you don't want something you've got, you are suffering.
- There is no self. The feeling of being separate from the world is an illusion.
- There is only the present moment, the past doesn't exist anymore, and the future doesn't exist yet.
