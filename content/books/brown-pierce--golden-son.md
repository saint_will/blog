---
title: Pierce Brown - Golden Son
date: 2020-08-28
started: 2020-08-23
goodreads: https://www.goodreads.com/book/show/18966819-golden-son
shelves:
  - fiction
  - sci-fi
---

Trilogy: [Red Rising](/books/pierce-brown-red-rising), Golden Son, [Morning Star](/books/pierce-brown-morning-star)

It would be hard not to spoil the first book when talking about what happened in the second, so I'll instead talk about some aspects of the trilogy that I love.

The story doesn't go into detail of how the science-fiction perks like terraforming, artificial gravity and their razors (light sabres) work. And so it boils down very much to a story about society and interpersonal relationships which is the most interesting part anyway.

The main hero Darrow is living in a high-class world where a low-born like himself shouldn't be. He's trying to infiltrate the society and bring a revolution. Yet, he's making solid friendships and romantic relationships. The story is of constant alliance and betrayal.

The book is very unpredictable. You never know who's going to turn out to be a great friend and who's going to betray the main hero.
