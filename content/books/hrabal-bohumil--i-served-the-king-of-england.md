---
title: "Bohumil Hrabal - I Served the King of England "
date: 2020-04-29
started: 2020-04-22
goodreads: https://www.goodreads.com/book/show/25018933-obsluhoval-jsem-anglick-ho-kr-le
shelves:
  - fiction
  - history
---

When I started reading this e-book, I thought that I've got to get a faulty file. Bohumil definitely wasn't messing around when starting this book. It's on since the page 1.

My impression of the book is of a downhill action packed ride like in the movies [Falling Down](https://www.imdb.com/title/tt0106856/), [Speed](https://www.imdb.com/title/tt0111257/?ref_=fn_al_tt_1) or [Crank](https://www.imdb.com/title/tt0479884/?ref_=nm_flmg_act_31). You get into a very eventful stream of experience of the main character Jan. Following his story from early teens to old age in one big sentence. And when I say sentence I mean it.

When Bohumil (the author) went to a primary school, they were teaching him about the letters, punctuation and sentences. After that he said:

![I've got this](https://i.imgflip.com/1qbj6g.jpg)

And went on writing this book. You could count the paragraphs on your fingers and sentences are pages long. But this unique style contributed to the "uninterrupted flow of events" experience.

The book is funny at times and really dark at others. As Jan (main character) gets older, the book gets more serious. I enjoyed reading it and would agree that it's one of the better book Czech literature has to offer.
