---
title: David Allen - Getting Things Done
date: 2020-07-08
started: 2020-06-07
goodreads: https://www.goodreads.com/book/show/22573850-getting-things-done
shelves:
  - non-fiction
  - self-help
  - productivity
---

This book had to be revolutionary during its time (2001), but even though the book was updated in 2015, it feels lagging in time. Especially the parts where the author focuses on in-trays and office supplies as the primary tools for staying organised are not useful. Those sections can be, in my opinion, completely skipped if you spend most of your day working with a computer.

The core ideas, on the other hand, are as valid as they ever were.

- Put everything from your head into a note-taking app
- Make sure that instead of generic “broken car” you track specific next actions “call Dave to get a contact for the car mechanic he recommended”
- Have distinct categories where you put a different type of information, so you don’t have to think about categorising
- Review all your outstanding work once a week so you can trust your system being up to date

Rather than reading the whole book as I did, I recommend reading an abstract (e.g. getAbstract.com) or one of the [good blog articles](https://hamberg.no/gtd) explaining the methodology (my own coming up too).
