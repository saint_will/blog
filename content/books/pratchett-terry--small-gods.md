---
title: "Terry Pratchett - Small Gods"
date: 2020-02-02
started: 2020-01-26
goodreads: https://www.goodreads.com/book/show/34484.Small_Gods
shelves:
  - fiction
  - fantasy
  - comedy
---

 I'm a firm believer in the God of lettuce. It's a safe bet I'm told.

The dynamic between a god needing his follower more than follower the god creates tricky and funny situations throughout the book. Of course, Pratchett's Discworld is a gift that keeps giving. Like for example the Omnians having the silly naive idea that world is a sphere circling around the sun, whilst we all well know it's a disc on backs of four elephants standing on a giant turtle (there's good eating on one of these).

My favourite character was a philosopher named Didactylos whose world view I'm planning on adopting.
