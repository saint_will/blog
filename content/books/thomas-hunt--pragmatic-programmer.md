---
title: "David Thomas, Andrew Hunt - The Pragmatic Programmer"
date: 2020-04-25
started: 2020-04-17
goodreads: https://www.goodreads.com/book/show/45280024-the-pragmatic-programmer
shelves:
  - non-fiction
  - software engineering
---

I have no idea, why did it take me 10 years of professional programming to get this book and read it. Maybe it was the 1999 release date that made it look too old. Too old to provide any useful advice. I'm therefore really happy that the authors decided to give that book a polish and release 20th anniversary edition.

This book instantly made it to top 5 technical books I've ever read and I now understand why the first edition had such a massive cult around it. It is well-rounded "How to be a software developer" guide that will provide value to both starting engineers and long time professionals.

It contains a good mix of soft, lofty ideas and hard practical coding advice. Both big picture and detailed oriented programmers will find their value in it.

One thing is for certain. This is not the last time I'm reading this book.
