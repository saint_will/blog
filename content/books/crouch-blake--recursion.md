---
title: "Blake Crouch - Recursion"
date: 2021-04-06
started: 2021-03-02
openlibrary: https://openlibrary.org/books/OL27274038M/Recursion
shelves:
  - fiction
  - sci-fi
---

<!-- I tried to roughly follow a guide to write a better review: https://www.grammarly.com/blog/how-to-write-book-review/ -->

I found this book because it was [voted the best Sci-Fi book of 2019 on Goodreads](https://www.goodreads.com/choiceawards/best-books-2019). After reading it, I agree with the vote.

The book describes a New York city struck by a false memory syndrome: people have memories from lives that didn't happen. The story's main protagonists are Helena, a scientist specializing in memory research and Barry, NYPD detective. As you read their story, you'll understand increasingly more about the mysterious mass memory issue.

The author did a great job of keeping suspense throughout the book and presenting the reader with information that they don't yet understand but will cause an "aha" moment later.

I particularly enjoyed the food-for-thought material in the book. Pondering the question: "What is memory?" and later on in the book: "What is life and how to live a good one?". Similarly to the [Semiosis](/books/burke-sue-semiosis/), I kept thinking about the story long after reading it.

<details>
<summary>Spolier section</summary>

After the story evolved and I understood that the whole book is one big [Groundhog Day](https://en.wikipedia.org/wiki/Groundhog_Day_(film)) it made me think about how I live my life. How every day is precious, and I shouldn't waste it. The sections where Berry and Helena relive their lives to spend more time with their sick family members motivated me to see my family.

I know that these "motivational" effects don't last long, and I'll forget them quickly, but it was a good motivation non-the-less.

</details>
