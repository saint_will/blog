---
title: "Burton G. Malkiel - A Random Walk Down Wall Street"
date: 2020-04-13
started: 2020-03-18
goodreads: https://www.goodreads.com/book/show/40242274-a-random-walk-down-wall-street
shelves:
  - non-fiction
  - finance
---

When I read Barefoot investor three years ago, I felt like I discovered a whole new world. Lowering taxes by saving for retirement, indexed funds? In my late twenties I finally started saving more intelligently.

Now the Random Walk took it a one step further. I actually understand the concepts behind the saving mechanisms and stock markets. I loved how the book encouraged critical thinking by describing smart concepts (authors of which often deserved Nobel Prize) and then showing their shortcomings.

As an engineer I love scientific approach and it is delight when Burton shows sometimes 30 years, other times 200 years of historical data to support his arguments.

The closing chapter that was talking about the obvious question *"What if everyone does indexing"* could be more detailed, because I still don't believe that the stock pricing would be efficient if the whole stock market is in indexed funds.
