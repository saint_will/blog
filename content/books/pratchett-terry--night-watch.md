---
title: "Terry Pratchett - Night Watch"
date: 2020-12-22
started: 2020-12-05
openlibrary: https://openlibrary.org/books/OL7480640M/Night_Watch
shelves:
  - fiction
  - fantasy
  - comedy
---

Time travelling done right. That's the book summed up in four words. As you can tell, I'm now a full-blown groupie of the Watch series. It would be as hard to disappoint me as having electronic conversations without funny GIFs.

This novel is all about the origin story of the watch. Just when you thought that the watch characters couldn't get any more colour and flavour, Sir Terry will take you 30 years back in time and gives you their detailed backstory. I refuse to accept that there are only two books left in the Watch series after this one.
