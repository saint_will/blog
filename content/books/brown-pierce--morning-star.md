---
title: Pierce Brown - Morning Star
date: 2020-08-31
started: 2020-08-29
goodreads: https://www.goodreads.com/book/show/18966806-morning-star
shelves:
  - fiction
  - sci-fi
---

Trilogy: [Red Rising](/books/pierce-brown-red-rising), [Golden Son](/books/pierce-brown-golden-son), Morning Star

The third book in the Red Rising Saga is a bit repetitive end you can see some of the plot twists coming from miles away. But the book wasn't a disappointment. It made for a good ending to the trilogy.

The catch? The trilogy is now [pentalogy](https://www.goodreads.com/series/117100-red-rising-saga). But I'm going to end with the third book because it brings a nice ending to the story and there is only so much time I can spend binge-reading sci-fi soap operas.
