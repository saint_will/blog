---
title: "Sue Burke - Semiosis"
date: 2020-01-16
started: 2019-12-23
goodreads: https://www.goodreads.com/book/show/35018907-semiosis
shelves:
  - fiction
  - sci-fi
---

I've never read a sci-fi quite like this. Semiosis tells a story in a way that will give you both detail and large overview of the idea.

Detail because the story is told by people (and plants) and large overview because the stories are told by different people in different generations of settlers on Pax.

The book stretched my vocabulary because there is a lot of chemistry and botanic terms that you don't come across in literature unless it's your field of study.

This book shows well the issue of information retention and how small population can't afford to stay as specialized as people are in today's economy and hence has to lower their understanding and use of technology. How long would our electronics and machine last if we lost the collective knowledge of the global world? 100 years? 200?
