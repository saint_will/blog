---
title: "Randall Munroe - How To: Absurd Scientific Advice for Common Real-World Problems"
date: 2020-01-17
started: 2020-01-08
goodreads: https://www.goodreads.com/book/show/42086897-how-to
shelves:
  - non-fiction
  - science
  - comedy
---

Is it a fiction, is it non-fiction? Who knows, who cares. This book looks long but thanks to the rich imagery I chewed through it in no time. The clarity of Munroe's explanation makes me feel smart. He only slipped once when he was explaining the hit precision for different ball sports but maybe that was just because he got me used to spoon feeding of facts in a very clear manner.

I laughed out loud multiple times when reading How To. Once uncontrollably. You'll know when you read the advice on how to loose election.

If my physics teachers used just one of these examples during their classes, I'd most likely went on a career in theoretical physics. I have to say: "Thank Jesus they didn't". Because engineering is much more fun.

If you have a river near by that you'd like to cross, do yourself a favour and do it as Randall says.
