# My library

I love reading both fiction and non-fiction books. Here I keep a short review for each book I finish. This section is mainly for my benefit, but write me a message if you'd like to talk about books.
