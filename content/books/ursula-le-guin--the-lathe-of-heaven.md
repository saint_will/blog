---
title: "Ursula K. Le Guin - The Lathe of Heaven"
date: 2021-05-30
started: 2021-04-20
openlibrary: https://openlibrary.org/works/OL59858W/The_Lathe_of_Heaven
shelves:
  - fiction
  - sci-fi
  - science
---
This novel is about dreams and reality. Usually, these are clearly separated but not in Portland, Oregon, not for George Orr, not in 1971.

The whole story happens in a dystopian "future" where humanity suffers from overpopulation and climate change. Ursula projected the worries of the sixties into this book well: overpopulation, declining environment and the cold war.

Overall, the book was OK but not great. I can't but help to compare with [Recursion](/books/crouch-blake-recursion/) - many details in the novel seemed not thought-through.

<details>
<summary>Details I struggled with (spoilers)</summary>

- Orr was portrayed as incredibly stable, thoughtful and down-to-earth, yet he let Dr. Harber take the world to the brink of destruction because he didn't want to go to prison.
- The way how other people (Harber, Heather) remembered different realities was inconsistent. There was no logic to it.
- The final scene when everything started falling apart in Harber's dream didn't make any sense.

</details>

Logic has to be the struggle for every time travelling/reality changing story. The fact that the novel is old is likely to be responsible for my lack of enthusiasm. I might have read improved versions of the same type of story. Now I can't appreciate the first but imperfect book.
